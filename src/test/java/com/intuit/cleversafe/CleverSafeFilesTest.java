package com.intuit.cleversafe;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Random;

import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Test;

public class CleverSafeFilesTest {

    @Test
    public void compressAndEncryptFiles() throws Exception {

        CleverSafeFiles fileUtil = new CleverSafeFiles();

        // Create test files
        File testFile1 = File.createTempFile("testFile1", null);
        testFile1.deleteOnExit();
        File testFile2 = File.createTempFile("testFile2", null);
        testFile2.deleteOnExit();
        File testTarFile = File.createTempFile("testTarFile", ".tar");
        testTarFile.deleteOnExit();
        String encryptedFileName = testTarFile.getPath();
        StringBuilder newFileName = (new StringBuilder(encryptedFileName)).append(".gpg");
        encryptedFileName = newFileName.toString();
        File encryptedFile = new File(encryptedFileName);
        encryptedFile.deleteOnExit();

        // Use test public key
        File testAscFile = fileUtil.getResourceFile("testPubKey.asc");

        // Fill test files with random data
        FileWriter writer1 = new FileWriter(testFile1);
        FileWriter writer2 = new FileWriter(testFile2);

        Random rnd = new Random();
        byte[] junk1 = new byte[ (int)Math.pow(10, 6) ];
        byte[] junk2 = new byte[ (int)Math.pow(10, 6) ];
        rnd.nextBytes(junk1);
        rnd.nextBytes(junk2);

        writer1.write(new String(junk1));
        writer1.close();
        writer2.write(new String(junk2));
        writer2.close();

        // Add test files to List object to use to add to tar and encrypt
        ArrayList<File> fileList = new ArrayList<File>();
        fileList.add(testFile1);
        fileList.add(testFile2);

        // Call utility to tar and encrypt test files
        fileUtil.compressAndEncryptFiles(fileList, testTarFile, testAscFile, true);
        assert encryptedFile.exists();

    }

    @Test
    public void compressAndEncryptFilesProdKey() throws Exception {

        CleverSafeFiles fileUtil = new CleverSafeFiles();

        // Create test files
        File testFile1 = File.createTempFile("testFile1", null);
        testFile1.deleteOnExit();
        File testFile2 = File.createTempFile("testFile2", null);
        testFile2.deleteOnExit();
        File testTarFile = File.createTempFile("testTarFile", ".tar");
        testTarFile.deleteOnExit();
        String encryptedFileName = testTarFile.getPath();
        StringBuilder newFileName = (new StringBuilder(encryptedFileName)).append(".gpg");
        encryptedFileName = newFileName.toString();
        File encryptedFile = new File(encryptedFileName);
        encryptedFile.deleteOnExit();

        // Use test public key
        File testAscFile = fileUtil.getResourceFile("testPubKey.asc");

        // Fill test files with random data
        FileWriter writer1 = new FileWriter(testFile1);
        FileWriter writer2 = new FileWriter(testFile2);

        Random rnd = new Random();
        byte[] junk1 = new byte[ (int)Math.pow(10, 6) ];
        byte[] junk2 = new byte[ (int)Math.pow(10, 6) ];
        rnd.nextBytes(junk1);
        rnd.nextBytes(junk2);

        writer1.write(new String(junk1));
        writer1.close();
        writer2.write(new String(junk2));
        writer2.close();

        // Add test files to List object to use to add to tar and encrypt
        ArrayList<File> fileList = new ArrayList<File>();
        fileList.add(testFile1);
        fileList.add(testFile2);

        // Call utility to tar and encrypt test files
        fileUtil.compressAndEncryptFiles(fileList, testTarFile, true);
        assert encryptedFile.exists();

    }

    @Test
    public void encryptFile() throws Exception {
        CleverSafeFiles fileUtil = new CleverSafeFiles();

        // Create test files
        File testFile1 = File.createTempFile("testFile1", null);
        testFile1.deleteOnExit();

        // Use test public key
        File testAscFile = fileUtil.getResourceFile("testPubKey.asc");
        PGPPublicKey pubKey = fileUtil.readPublicKey(testAscFile);

        fileUtil.encryptFile(testFile1, pubKey);

        //TODO: validate file is actually encrypted, for now no exception is enough
    }

    @Test
    public void readPublicKey() throws Exception {
        CleverSafeFiles fileUtil = new CleverSafeFiles();

        // Use test public key
        File testAscFile = fileUtil.getResourceFile("testPubKey.asc");
        PGPPublicKey pubKey = fileUtil.readPublicKey(testAscFile);

        assert pubKey instanceof PGPPublicKey;
    }

    @Test
    public void addFileToTarGz() throws Exception {
        CleverSafeFiles fileUtil = new CleverSafeFiles();

        // Create test files
        File testFile1 = File.createTempFile("testFile1", null);
        testFile1.deleteOnExit();
        File testFile2 = File.createTempFile("testFile2", null);
        testFile2.deleteOnExit();
        File testTarFile = File.createTempFile("testTarFile", ".tar");
        testTarFile.deleteOnExit();

        // Fill test files with random data
        FileWriter writer1 = new FileWriter(testFile1);
        FileWriter writer2 = new FileWriter(testFile2);

        Random rnd = new Random();
        byte[] junk1 = new byte[ (int)Math.pow(10, 6) ];
        byte[] junk2 = new byte[ (int)Math.pow(10, 6) ];
        rnd.nextBytes(junk1);
        rnd.nextBytes(junk2);

        writer1.write(new String(junk1));
        writer1.close();
        writer2.write(new String(junk2));
        writer2.close();

        // Add test files to List object to use to add to tar and encrypt
        ArrayList<File> fileList = new ArrayList<File>();
        fileList.add(testFile1);

        fileUtil.addFilesToTarGz(testTarFile, fileList);

        BufferedReader br = new BufferedReader(new FileReader(testTarFile));
        if (br.readLine() == null) {
            throw new Exception("Tar file empty");
        }

    }

    @Test
    public void validateDecryptOfTar() throws Exception{
        //TODO: decrypt and validate output of compressAndEncryptFiles (<TARFILE>.tar.pgp)
        throw new Exception("Unimplemented test") ;
    }
}