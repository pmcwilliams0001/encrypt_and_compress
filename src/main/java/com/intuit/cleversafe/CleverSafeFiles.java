package com.intuit.cleversafe;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcePGPDataEncryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyKeyEncryptionMethodGenerator;

import java.io.*;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Iterator;
import java.util.List;

public class CleverSafeFiles {

    static final int COMPRESSION_ALGORITHIM = 1;
    static final int ENC_ALGORITHIM = 3;
    static final boolean USE_INTEGRITY_PACKET = true;
    static final String DEFAULT_PUB_KEY_FILENAME = "IntuitPublic.asc";
    private TarArchiveOutputStream tarArchiveStream = null;

    /**
     * Nexidia public key is already included, this method can be used to include the default public key
     * @param files List of files to be added to the tar file
     * @param tarFile Tar file that will contain all files and be encrypted
     * @throws IOException
     * @throws PGPException
     */
    public void compressAndEncryptFiles (List<File> files, File tarFile) throws IOException, PGPException {
        this.compressAndEncryptFiles(files, tarFile, false);
    }

    /**
     * When supplying a PGP public key file use this method
     * @param files List of files to be added to the tar file
     * @param tarFile Tar file that will contain all files and be encrypted
     * @param ascFile PGP public key file to be used to encrypt the tar file
     * @throws IOException
     * @throws PGPException
     */
    public void compressAndEncryptFiles (List<File> files, File tarFile, File ascFile) throws IOException, PGPException {
        this.compressAndEncryptFiles(files, tarFile, ascFile, false);
    }

    /**
     * When supplying a PGP public key as a string use this method
     * @param files List of files to be added to the tar file
     * @param tarFile Tar file that will contain all files and be encrypted
     * @param pubKeyAsString PGP public key as string to be used to encrypt the tar file
     * @throws IOException
     * @throws PGPException
     */
    public void compressAndEncryptFiles (List<File> files, File tarFile, String pubKeyAsString) throws IOException, PGPException {
        this.compressAndEncryptFiles(files, tarFile, pubKeyAsString, false);
    }


    protected void compressAndEncryptFiles (List<File> files, File tarFile, boolean outputUnencrypted) throws IOException, PGPException {
        PGPPublicKey pubKey = null;
        File ascFile = this.getResourceFile(DEFAULT_PUB_KEY_FILENAME);
        pubKey = readPublicKey(ascFile);
        compressAndEncryptFiles(files, tarFile, pubKey, outputUnencrypted);
    }

    protected void compressAndEncryptFiles (List<File> files, File tarFile, File ascFile, boolean outputUnencrypted) throws IOException, PGPException {
        PGPPublicKey pubKey = null;
        pubKey = readPublicKey(ascFile);
        compressAndEncryptFiles(files, tarFile, pubKey, outputUnencrypted);
    }

    protected void compressAndEncryptFiles (List<File> files, File tarFile, String pubKeyAsString, boolean outputUnencrypted) throws IOException, PGPException {
        PGPPublicKey pubKey = null;
        pubKey = readPublicKey(pubKeyAsString);
        compressAndEncryptFiles(files, tarFile, pubKey, outputUnencrypted);
    }

    private void compressAndEncryptFiles (List<File> files, File tarFile, PGPPublicKey pubKey, boolean outputUnencrypted) throws IOException, PGPException {
        addFilesToTarGz(tarFile, files);
        String filePath = tarFile.getPath();
        StringBuilder newFileName = (new StringBuilder(filePath)).append(".gpg");
        File renamedFile = new File(newFileName.toString());
        copyFile(tarFile, renamedFile);
        tarFile = renamedFile;
        if (outputUnencrypted){
            File unencryptedTar = new File(filePath);
            unencryptedTar.createNewFile();
            copyFile(tarFile, unencryptedTar);
        }

        this.encryptFile(tarFile, pubKey);
    }

    protected void encryptFile(File fileToEncrypt, PGPPublicKey pubKey) throws IOException, PGPException {
        byte[] compressedFile = compressFile(fileToEncrypt);
        long compressedFileLength = (long)compressedFile.length;
        JcePGPDataEncryptorBuilder encryptorBuilder = new JcePGPDataEncryptorBuilder(ENC_ALGORITHIM);
        encryptorBuilder
                .setWithIntegrityPacket(USE_INTEGRITY_PACKET)
                .setSecureRandom( new SecureRandom() )
                .setProvider("BC");

        PGPEncryptedDataGenerator encryptedDataGenerator = new PGPEncryptedDataGenerator(encryptorBuilder);

        JcePublicKeyKeyEncryptionMethodGenerator jcePubMethodGen = new JcePublicKeyKeyEncryptionMethodGenerator(pubKey);
        jcePubMethodGen.setProvider("BC");
        encryptedDataGenerator.addMethod(jcePubMethodGen);

        BufferedOutputStream bufferedOut = new BufferedOutputStream( new FileOutputStream(fileToEncrypt) );
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        OutputStream encryptedOut = encryptedDataGenerator.open(bufferedOut, compressedFileLength);
        encryptedOut.write(compressedFile);
        encryptedOut.close();
        bufferedOut.close();
    }

    protected static PGPPublicKey readPublicKey(String pubKeyAsString) throws IOException, PGPException {
        InputStream pubKeyStream = new ByteArrayInputStream(pubKeyAsString.getBytes());
        PGPPublicKey pubKey = readPublicKey( pubKeyStream );
        return pubKey;
    }

    protected static PGPPublicKey readPublicKey(File ascFile) throws IOException, PGPException {
        InputStream pubKeyStream = new FileInputStream(ascFile);
        PGPPublicKey pubKey = readPublicKey( pubKeyStream );
        return pubKey;
    }

    private static PGPPublicKey readPublicKey(InputStream pubKeyStreamIn) throws IOException, PGPException {
        InputStream pubKeyStream = PGPUtil.getDecoderStream(pubKeyStreamIn);
        JcaKeyFingerprintCalculator jcaKeyFingerprintCalculator = new JcaKeyFingerprintCalculator();
        PGPPublicKeyRingCollection publicKeyRings = new PGPPublicKeyRingCollection(pubKeyStream, jcaKeyFingerprintCalculator);

        Iterator var2 = publicKeyRings.getKeyRings();

        while(var2.hasNext()) {
            PGPPublicKeyRing var3 = (PGPPublicKeyRing)var2.next();
            Iterator var4 = var3.getPublicKeys();

            while(var4.hasNext()) {
                PGPPublicKey pubKey = (PGPPublicKey)var4.next();
                if (pubKey.isEncryptionKey()) {
                    pubKeyStreamIn.close();
                    pubKeyStream.close();
                    return pubKey;
                }
            }
        }

        pubKeyStreamIn.close();
        pubKeyStream.close();
        throw new IllegalArgumentException("Can't find encryption key in key ring.");
    }

    private static byte[] compressFile(File fileToEncrypt) throws IOException {
        ByteArrayOutputStream var2 = new ByteArrayOutputStream();
        PGPCompressedDataGenerator var3 = new PGPCompressedDataGenerator(COMPRESSION_ALGORITHIM);
        PGPUtil.writeFileToLiteralData(var3.open(var2), 'b', fileToEncrypt);
        var3.close();
        return var2.toByteArray();
    }

    protected void addFilesToTarGz(File tarFile, List<File> filesToAdd) throws IOException {
        addFilesToTarGz(tarFile, filesToAdd, "");
    }

    private void addFilesToTarGz(File tarFile, List<File> filesToAdd, String base) throws IOException {
        TarArchiveOutputStream tOut = this.getTarArchiveStream(tarFile);
        for(File fileToAdd : filesToAdd) {
            String entryName = base + fileToAdd.getName();
            TarArchiveEntry tarEntry = new TarArchiveEntry(fileToAdd, entryName);
            tOut.putArchiveEntry(tarEntry);

            IOUtils.copy(new FileInputStream(fileToAdd), tOut);
            tOut.closeArchiveEntry();
        }
        this.closeTarArchive();

    }


    private TarArchiveOutputStream getTarArchiveStream(File tarFile) throws IOException {
        FileOutputStream fOut;
        BufferedOutputStream  bOut;
        GzipCompressorOutputStream gzOut;
        if (this.tarArchiveStream != null){
            return this.tarArchiveStream;
        }

        fOut = new FileOutputStream(tarFile);
        bOut = new BufferedOutputStream(fOut);
        gzOut = new GzipCompressorOutputStream(bOut);
        this.tarArchiveStream = new TarArchiveOutputStream(gzOut);
        return this.tarArchiveStream;

    }

    private void closeTarArchive() throws IOException {
        TarArchiveOutputStream stream = this.tarArchiveStream;
        stream.finish();
        stream.close();
    }

    private static void copyFile(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }

    protected File getResourceFile(String fileName){
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());
        return file;
    }
}

